#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

declare debug_mode=${SWOOSHIT_DEBUG:-true}
${debug_mode} && declare debug_filename="${0%.*}.debug"
${debug_mode} && debug_log() { #1: Message, #2: Filename
    # TODO: Write a generic ${debug_mode} && debug_log function in ubf.sh that return all frames via the caller builtin
    local message="${1:-[${BASHPID}] $(caller)}"
    local filename="${2:-${debug_filename}}"
    printf "[%s] [%7s] [%-24s] %s\n" "$(date -Is)" "${BASHPID}" "$(caller)" "${message}" | tee -a "${filename}" >&2
}
export debug_mode
export debug_filename
export -f debug_log
is10int() {
    [[ ${1} = 0        ]] && return 0
    [[ ${1} =~ ^-?[0]  ]] && return 1
    [[ ${1} =~ [^-0-9] ]] && return 1
    return 0
}; export -f is10int
is10pint() {
    [[ ${1} = 0       ]] && return 0
    [[ ${1} =~ ^0     ]] && return 1
    [[ ${1} =~ [^0-9] ]] && return 1
    return 0
}; export -f is10pint
do_bc() { #1: expression, [#2: scale (8 by default)]
    result=$(echo "scale=${2:-8};${1}" | bc -lq)
    echo "${result}"
}; export -f do_bc;
bc_test() { #1: expression
    test $(do_bc "${1}") -eq 1
}; export -f bc_test
abort() {
    ${debug_mode} && debug_log "${1:-Illegal argument(s)}"
    exit ${2:-2}
}; export -f abort

ran() { cat /dev/urandom | tr --complement -d "${1:-0-9}" | head -c${2:-1}; }; export -f ran

get_duration() { sox --i -D "${1}"; }; export -f get_duration

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    echo "Add a “source ./${0}” in the script in which you want to use those functions" >&2
fi
