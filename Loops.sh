#!/bin/bash

# WIP: Shit Sample Generator

# All the following return codes are not actually used. It’s here for quick reference and the script tries to respect this convention.
# https://tldp.org/LDP/abs/html/exitcodes.html
# 1 Catchall for general errors let "var1 = 1/0" Miscellaneous errors, such as "divide by zero" and other impermissible operations
# 2 Misuse of shell builtins (according to Bash documentation) empty_function() {} Missing keyword or command, or permission problem (and diff return code on a failed binary file comparison).
# 126 Command invoked cannot execute /dev/null Permission problem or command is not an executable
# 127 "command not found" illegal_command Possible problem with $PATH or a typo
# 128 Invalid argument to exit exit 3.14159 exit takes only integer args in the range 0 - 255 (see first footnote)
# 128+n Fatal error signal "n" kill -9 $PPID of script $? returns 137 (128 + 9)
# 130 Script terminated by Control-C Ctl-C Control-C is fatal error signal 2, (130 = 128 + 2, see above)
# 255* Exit status out of range exit -1 exit takes only integer args in the range 0 - 255

set +x
set -euo pipefail
IFS=$'\n\t'

## Show help
function show_help { cat<<EOF

Usage: $(basename ${0}) [-h/--help] [-v/--verbose] [-d/--debug] [-g/--glob <glob>] [-r/--regex <regex>] [-w/--where <path>]
            [-c/--cycle-params "<start value> <coefficient> <modulo value>"]
            [-b/--bpm <BPM>] [-l/--length <sample length (beat)>] [-N/--normalize <attenuation (dB)>] [-P/--pad <duration (s)>]
            [-R/--sample-rate <value (Hz)>] [-B/--bit-rate <bit>] [-C/--channels <nb of channels>]

EOF
}

function handle_args {
    declare -g  verbose=""
    declare -g  debug=""
    declare -g  where=""
    declare -g  glob=""
    declare -g  regex=""
    declare -g  bpm=""
    declare -g  length=""
    declare -g  cycle_params=""
    declare -g  osr=""
    declare -g  obr=""
    declare -g  onc=""
    declare -g  normalize=""
    declare -g  otype=""
    declare -g  oname=""
    declare -g  pad=""
    
    for argument in "${@}"; do
      shift
      case "${argument}" in
         ('--verbose')             set -- "${@}" '-v' ;;
         ('--debug')               set -- "${@}" '-d' ;;
         ('--help')                set -- "${@}" '-h' ;;
         ('--glob')                set -- "${@}" '-g' ;;
         ('--regex')               set -- "${@}" '-r' ;;
         ('--bpm')                 set -- "${@}" '-b' ;;
         ('--length')              set -- "${@}" '-l' ;;
         ('--cycle-params')        set -- "${@}" '-c' ;;
         ('--where')               set -- "${@}" '-w' ;;
         ('--sample-rate')         set -- "${@}" '-R' ;;
         ('--bit-rate')            set -- "${@}" '-B' ;;
         ('--channels')            set -- "${@}" '-C' ;;
         ('--normalize')           set -- "${@}" '-N' ;;
         ('--type')                set -- "${@}" '-t' ;;
         ('--name')                set -- "${@}" '-n' ;;
         ('--pad')                 set -- "${@}" '-P' ;;
         (*)                       set -- "${@}" "${argument}"
      esac
    done
    while getopts "vhdg:r:b:l:c:w:R:B:C:N:t:n:P:" option; do
        case "${option}" in
            (v)  declare -g  verbose=true              ;;  # Write some information on the standard output
            (d)  declare -g  debug=true                ;;  # Write some information on the error output
            (w)  declare -g  where="${OPTARG}"         ;;  # Directory to search in, default is current directory
            (g)  declare -g  glob="${OPTARG}"          ;;  # Sample files by glob, default is *
            (r)  declare -g  regex="${OPTARG}"         ;;  # Sample files by a regex, default is .*
            (b)  declare -g  bpm="${OPTARG}"           ;;  # Reference BPM, default is 140.
            (l)  declare -g  length="${OPTARG}"        ;;  # Length of the generated sample(s) in number of beat, default is 1
            (c)  declare -g  cycle_params="${OPTARG}"  ;;  # Parameters for cycle generation: start, coefficient, modulo, operation
            (R)  declare -g  osr="${OPTARG}"           ;;  # Output file sample-rate, default is 96 kHz
            (B)  declare -g  obr="${OPTARG}"           ;;  # Output file bit-rate, default is 24 bit
            (C)  declare -g  onc="${OPTARG}"           ;;  # Number of channels, default is 1 (Mono)
            (N)  declare -g  normalize="${OPTARG}"     ;;  # normalization value, default is -3 (-3 dB)
            (t)  declare -g  otype="${OPTARG}"         ;;  # Outpout type, default is "alsa" (ie: soundcard)
            (n)  declare -g  oname="${OPTARG}"         ;;  # Outpout name, default is "default" if type is "alsa" or "YYYYmmDDHHMMSS-PID.type" else
            (P)  declare -g  pad="${OPTARG}"           ;;  # pädding silence between sample
            (h)  show_help; exit 0                     ;;  # User asked for the help message, then show help and exit OK
            (\?) show_help; exit 2                         # An unknown option has been given, then show help and exit with 2
        esac
    done
    
    # Use some default values if none provided
    test -z "${regex}" && test -z "${glob}" && declare -gr glob='*'
    test -z "${bpm}"                        && declare -gr bpm=140
    test -z "${where}"                      && declare -gr where='.'
    test -z "${length}"                     && declare -gr length=4
    test -z "${cycle_params}"               && declare -gr cycle_params='0 0 0'
    test -z "${otype}"                      && declare -gr otype='alsa'
    test -z "${oname}"                      && declare -gr oname='default'
    test -z "${normalize}"                  && declare -gr normalize=-3
    test -z "${verbose}"                    && declare -gr verbose=false
    test -z "${debug}"                      && declare -gr debug=false

    # Validate/sanitize user provided values
    # TODO

    # Calculate some constants which depend of primary values
    declare -gr beat_duration="$(echo "scale=3; 1/(${bpm}/60)" | bc -lq | cut -d'.' -f2)"
}

function show_params {
    LANG=C
    printf "          Where: %-4s\n"        "${where}"           >&2
    printf "           Glob: %-4s\n"        "${glob}"            >&2
    printf "          Regex: %-4s\n"        "${regex}"           >&2
    printf "     Parameters: %-4s \n"       "${cycle_params}"    >&2
    printf "          Tempo: %-4i BPM\n"    "${bpm}"             >&2
    printf "         Length: %-4i bar\n"    "${length}"          >&2
    printf "  Beat duration: %-4i ms\n"     "${beat_duration}"   >&2
    printf "  Normalization: %-4s dB\n"     "${normalize}"       >&2
    printf "    Output type: %-4s\n"        "${otype}"           >&2
    printf "    Output name: %-4s\n"        "${oname}"           >&2
}

function do_exit {
    printf "\n\n%s\n" "Interrupted… Bye! _o/"
    exit 3
}
trap do_exit SIGINT

function ran10int { # 1: number of digits (5 if unspecified)
    head -z -c1 <(tr -dc '1-9' < /dev/urandom)
    head -z -c$(( ${1:-5} - 1 )) <(tr -dc '0-9' < /dev/urandom) 
}

function is_in { # 1: candidate, 2: indexed array
    for element in "${@:2}"; do
        test "${element}" = "${1}" && { echo -n "${element}"; return 0; }
    done
    return 1
}

function next {
    # next = current <operator> coefficient % modulo
    local ln=${1} # $nr, nameref to global $n
    local lc=${2}
    local lm=${3}
    local lo=${4:-*} # Multiply is the default operator if unspecified
    test ${lm} -eq 0 || { ln=$(((ln${lo}lc)%lm)); }
    echo -n ${ln}
}

function cycle { # 1: start number, 2: coefficient, 3: Modulo, 4: Type (arithmetic operator)
    local -i  gn=10#${1}
    local -n  nr=gn
    local -ir c=10#${2}
    local -ir m=10#${3}
    local -r  o="${4:-*}" # Multiply is the default operator if unspecified
    declare -ag  cycle=()
    while ! is_in "${gn}" "${cycle[@]}"; do
        cycle+=("${gn}")
        echo -n "${gn} "
        gn=$(next ${nr} ${c} ${m} "${o}")
    done
    cycle+=("${gn}")
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then # Do not execute if file is being sourced
    handle_args "${@}"
    ${verbose} && show_params
    declare -a params
    IFS=' ' read -a params <<<"${cycle_params}"
    cycle "${params[@]}"
    echo
    ${debug} && {
        printf "\nParameters     : "                     >&2
        printf "%s " "${params[@]}"                      >&2
        printf "\n"                                      >&2
        printf "Cycle length   : %-4i\n" "${#cycle[@]}"  >&2
        printf "Cycle          : "                       >&2
        printf "%-s " "${cycle[@]}"                      >&2
        printf "\n"                                      >&2
    }
fi

