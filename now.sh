#!/bin/bash
set -ueo pipefail
IFS=$'\t\n'

while getopts 'hvT:a:S:D:' option; do
    case "${option}" in
        (v) declare -r  opt_verbose=true                ;;   # Be verbose                             (false)
        (h) declare -r  opt_help=true                   ;;   # Show help                              (false)
        (T) declare -r  opt_tempo="${OPTARG}"           ;;   # Tempo                                  (140)
        (a) declare -r  opt_action="${OPTARG}"          ;;   # Action                                 (none)
        (S) declare -r  opt_steps_by_32th="${OPTARG}"   ;;   # How many position possible in a 32th   (6)  # Number of steps by 32th
        (D) declare -r  opt_duration="${OPTARG}"        ;;   # Duration in second                     (1)  # Loop for duration
    esac
done


# TODO: Have default value variabilized
show_usage() {
    cat<<EOF
Usage: ${0} [-h] [-v] [-T <tempo>] [-a <action>] [-S <step per 32th of bar>] [-D <duration>]

Defaults values:

 - Tempo:                 140   BPM
 - Steps per 32th of bar: 6     bar/32
 - Duration:              1     second

EOF
}
declare -r tempo=${opt_tempo:-140}
declare -r steps_by_32th=${opt_steps_by_32th:-6}
declare -r duration=${opt_duration:-1}

"${opt_help:-false}" && show_usage && exit 0

do_bc()       { echo "scale=${2:-9};${1}"         | bc -lq;        }
bc_test()     { test $(echo "${1}"                | bc -lq) -eq 1; }
shutdown()    { exit $?; }; trap shutdown 2

list_note_length() {
    # tempo whole half quarter eighth sixteenth 32th
    printf "%s %s %s %s %s %s %s %s %s\n" "${tempo}" "${w}" "${h}" "${q}" "${e}" "${s}" "${t}" "${step}" "${tick}"
}

now_ns() { date +%s%N; }
now_ms() { now_ns | cut --complement -c 14-; }
loop() { #1: number of seconds
    #~ local duration_ns=$((${1:-1}*1000000000))
    local duration_ms=$((${1:-1}*1000))
    #~ local -r start_ns="$(date +%s%N)"
    local -r start_ms="$(now_ms)"
    local -i t_index=0
    local -i time=$(now_ms)
    local -i e=0
    while [[ $((time-start_ms)) -lt ${duration_ms} ]]; do
        time=$(now_ms)
        e=$((time%t))
        #~ echo -n "time=${time} e=${e} "
        [[ ${e} -le 1 ]] && {
            #~ echo "32th: $((++t_index))"
            t_index=$t_index+1
            }
    done
    echo $t_index
}


## 4/4 signature assumed
# quarter       1/(tempo/60)    → q
# half          2*quarter       → h
# whole         2*half          → w
# eighth        quarter/2       → e
# sixteenth     eighth/2        → s
# 32th          sixteenth/2     → t

#~ set -x

#~ q="$(do_bc "1000000000/(${tempo}/60)" 0)" # quarter note nanosecond
#~ h="$(do_bc "2*${q}" 0)"
#~ w="$(do_bc "2*${h}" 0)"
#~ e="$(do_bc "${q}/2" 0)"
#~ s="$(do_bc "${e}/2" 0)"
#~ t="$(do_bc "${s}/2" 0)"
#~ step="$(do_bc "${t}/${steps_by_32th}" 0)"

q="$(do_bc "1000/(${tempo}/60)" 0)" # quarter note millisecond
h="$(do_bc "2*${q}" 0)"
w="$(do_bc "2*${h}" 0)"
e="$(do_bc "${q}/2" 0)"
s="$(do_bc "${e}/2" 0)"
t="$(do_bc "${s}/2" 0)"
step="$(do_bc "${t}/${steps_by_32th}" 0)"


test "${opt_action:-none}" = 'list' && list_note_length
test "${opt_action:-none}" = 'loop' && loop "${duration}"

#~ set +x

