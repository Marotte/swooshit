# Swooshit TODO

## Fix

 - Need more clean handling of some error cases
 - Sanitize command line arguments and provide some useful error message then
 - Handle SoX fails when some parameters aren’t valid (ex: fede-in/fade-out overlap)
 - tests.sh: fix the broken test and implement some more 
 - Handle the case when using the -n/--name option without -t/--type (ignore)
 - CallResponse.sh: Do not run SoX when coefficient isn’t between 0.1 and 100
 - CallResponse.sh: add a clean_exit() on SIGTERM, EXIT, etc… 

## Functionalities

 - Exclusion of files on their name in search. Although it may probably be already possible if you’re a regex guru (egrep flavor). What I’m not.
 - “Humanization” (which is rather a per sample randomization)
 - Permit a randomization/humanization on a per parameter basis. Currently the amount of randomization can be set, but all parameters (for which it makes sens) are affected (except “Slide” I should implement soon)
 - Look at options --buffer and --multi-threading
 - Swoosher01: let the possibility to specify a stop time to fade so an effect like reverb do not stop at the end of the file
 - Swoosher01: possibility to give percentage values for fade-in fade-out so no need to adapt the value to sound length


## Documentation

 - French translation

### Help message

### README

