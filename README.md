# Swooshit

 :warning: Early development. Bad things should not happen, but could happen still.

Those scripts are wrappers for the sox audio manipulation program, aimed at generating some odd sound samples.

Documentation is sporadic, to say the least, and often obsolete as the development is anarchic (this is assumed and on purpose).

An example:

`
i=0; while [[ i++ -lt 128  ]]; do  ./Swoosher01  -w "${HOME}"   -b 144 -r '.*\.(mp3|wav|flac)' -e ""  -N-3 -l$(ranran 01)$(ranran 0-9) -S "$(echo $(ranran ' -') |tr -d ' ')$(ranran 0-9 4),$(echo $(ranran ' -') | tr -d ' ')$(ranran 0-9 3)"  -x.02 -E " reverse repeat 1" -k -P 1 -s1 -c1; done & 
`

The `ranran` function is define in the current shell as:

`
ranran () { tr -cd "${1:-0-9}" < /dev/urandom > >(head -c"${2:-1}");  }
`